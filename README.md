## komputer-store
<!-- blank line -->
## Name
komputer-store
<!-- blank line -->
## Maintainer
GitLab- @gabriel_andersson, 
GitLab- Gabriel Andersson
<!-- blank line -->
## Description
Assignment given by Noroff.

<!-- blank line -->

A project where the user can add funds to personal balance, request loans and ultimately buy the computer of their choice.
  - The loan cannot be more than twice the amount in the bank-balance. The user can only have one loan at a time.
  - Ten percent of the personal account balance will go directly to paying off an active loan - if the user tries to transfer funds 
	to the bank account.
<!-- blank line -->

## Installation
Since this project heavily relies on modules users need to add the live server extension (if using vscode) to run this website. 
	- Open vscode 
	- In the left vertical panel click the symbol that resembles four tiles(Extensions) 
	- Search for live server and install
	- Make sure you reload vscode. Now the "Go Live" button should appear. 
	
<!-- blank line -->

##Launching website: 
	1. Fetch this repo 
	2. Open vscode and then choose the repo folder.
	3. in the lower right bottom you should be able to spot a "Go Live" button, click on it. (if it doesn't appear  



## Authors and acknowledgment
@gabriel_andersson
