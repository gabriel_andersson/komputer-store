import { Updater } from "./updater.js";

//Instantiating defaultComputer in order to send the first computer in the api-array to the method in case a user clicks the "buy now" button. 
let defaultComputer = {};

//Fetching the api 
let computers = await getComputers().catch(error => {
    console.log(error.message);
});

//Iterating over and populating "option" elements to the select element. Also setting default data so that some the first computers data is always up on display.
for (const computer of computers) {

    Updater.updateElement("desc", computers[0].description);
    Updater.updateElement("name", computers[0].title);
    Updater.formatAndUpdateMoneyElement("price", computers[0].price);

    document.getElementById("img").src = `https://noroff-komputer-store-api.herokuapp.com/${computers[0].image}`;

    const newComputer = document.createElement("option");
    newComputer.value = computer.id;
    newComputer.text = computer.title;

    document.getElementById("laptop-select").appendChild(newComputer);
}

//Listen to changes in select box and populate the html elements with the selected computers attributes 
document.getElementById("laptop-select").addEventListener("change", (computer) => {

    //Setting the defaultComputer to the currently selected computer. Then it gets imported to index.js 
    let selectedComputer = computers[computer.target.selectedIndex];
    defaultComputer = selectedComputer;

    Updater.updateElement("desc", selectedComputer.description);
    Updater.updateElement("name", selectedComputer.title);
    Updater.formatAndUpdateMoneyElement("price", selectedComputer.price);

    //Fixing image urls
    if (selectedComputer.image === "assets/images/5.jpg") {

        document.getElementById("img").src = "https://noroff-komputer-store-api.herokuapp.com/assets/images/5.png";
    } else {

        document.getElementById("img").src = `https://noroff-komputer-store-api.herokuapp.com/${selectedComputer.image}`;
    }
});

//Calls api and fetches the komputer-store-api. The func also logs an error message if the response code was not in the 200's-range.
async function getComputers() {

    const response = await fetch("https://noroff-komputer-store-api.herokuapp.com/computers");
    if (!response.ok) {
        const message = `An error has occured: ${response.status}`;
        throw new Error(message);
    }
    const computers = await response.json();
    return computers;
}


export { getComputers, defaultComputer, computers };




