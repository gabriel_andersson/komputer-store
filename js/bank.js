import { Validator } from "./validation.js";
import { Updater } from "./updater.js";

class Bank {
    constructor() {
        this.bankBalance = 0;
        this.loanValue = 0;
    }

    // Transfer from personal balance account to bank account
    bankTransfer(person) {
        //Case1: Person has loan and the 10% of the salary is bigger or equal to the remainder of the loan.
        if (person.hasLoan) {
            if ((person.Bank.loanValue - person.personalEconomy * 0.1) <= 0) {

                let remainder = person.personalEconomy - person.personalEconomy * 0.1;
                person.Bank.loanValue = 0;
                person.personalEconomy = 0;
                person.Bank.bankBalance += remainder;
                person.hasLoan = false;

                Updater.formatAndUpdateMoneyElement("displaySalary", person.personalEconomy);
                Updater.formatAndUpdateMoneyElement("displayBankBalance", person.Bank.bankBalance);

                Updater.hideElement("loanBtn");
                Updater.hideElement("loanBalance");
            }
            //Case2: Person has loan and the 10% pays off a part of the loan.
            else {

                person.Bank.loanValue -= person.personalEconomy * 0.1;
                person.Bank.bankBalance += person.personalEconomy * 0.9;
                person.personalEconomy = 0;

                Updater.formatAndUpdateMoneyElement("displaySalary", person.personalEconomy);
                Updater.formatAndUpdateMoneyElement("displayBankBalance", person.Bank.bankBalance);
                Updater.formatAndUpdateMoneyElement("loanBalance", person.Bank.loanValue);
            }
        }
        //Case3: Person has no previous loan. All funds on the personal balance account gets transfered to the bank balance account.
        person.Bank.bankBalance += person.personalEconomy;
        person.personalEconomy = 0;

        Updater.formatAndUpdateMoneyElement("displaySalary", person.personalEconomy);
        Updater.formatAndUpdateMoneyElement("displayBankBalance", person.Bank.bankBalance);
    }

    //Static formatting function that you can call on Bank directly, wouldn't make any sense to have this func on an instance of the class
    //i chose to give the Bank this responsibility since banks usually handle moneyrelated issues
    static formatCurrency = (input) => {
        let formattedNumber = new Intl.NumberFormat("sv-SE", { style: "currency", currency: "SEK" }).format(input);
        return formattedNumber;
    }

    //Delegating responsibility of input, validation error messages and setting the dom elements to new vars in the event listener.
    //Here i'm trying to implement the early return "pattern" in order to make the code more clean/getting rid of some else blocks. 
    getLoan(person, loanInput) {

        if (person.hasLoan) {
            return [null, "You already have a loan!"];
        }
        if (!Validator.validateLoanUserInput(loanInput)) {
            return [null, "Invalid input!"];
        }
        if (!Validator.isLoanWithinBounds(this.bankBalance, loanInput)) {
            return [null, "Not a valid amount, the loan can't be bigger than twice the amount of the bank balance"];
        }

        person.hasLoan = true;
        this.loanValue = Number(loanInput);
        person.Bank.bankBalance += Number(loanInput);
        return [this.loanValue, null];
    }
}

export { Bank };