import { Bank } from "./bank.js";
import { Updater } from "./updater.js";

class Person {
    constructor() {
        this.personalEconomy = 0;
        this.totalMoneyEarned = 0;
        this.Bank = new Bank();
        this.hasLoan = false;
    }

    // Adds 100 SEK to persons personal balance account
    addSalary(person) {

        person.personalEconomy += 100;
        person.totalMoneyEarned += 100;

        Updater.formatAndUpdateMoneyElement("displaySalary", person.personalEconomy);
        Updater.formatAndUpdateMoneyElement("displayTotalEarned", person.totalMoneyEarned);
    }

    //Withdrawing money from the personal balance account and subtracting from the loaned amount of money from Bank. 
    static repayLoan(person) {
        if (person.Bank.loanValue - person.personalEconomy < 0) {

            let remainder = person.personalEconomy - person.Bank.loanValue;
            person.Bank.loanValue = 0;
            person.personalEconomy = remainder;
            person.hasLoan = false;

            Updater.hideElement("loanBtn", "none"); 
            Updater.hideElement("loanBalance", "none");
            Updater.formatAndUpdateMoneyElement("displaySalary", person.personalEconomy);
        }
        //Case: Person pays of the exact sum of the loan instantly. Differs a bit from the former case since there's no money left in the pesonal balance this time.
        else if (person.Bank.loanValue - person.personalEconomy === 0) {

            person.Bank.loanValue = 0;
            person.personalEconomy = 0;
            person.hasLoan = false;

            Updater.hideElement("loanBtn");
            Updater.hideElement("loanBalance");
            Updater.formatAndUpdateMoneyElement("displaySalary", person.personalEconomy);
        }
        //Case: Person pays of part of loan.
        else {
            person.Bank.loanValue -= person.personalEconomy;
            person.personalEconomy = 0;

            Updater.formatAndUpdateMoneyElement("loanBalance", person.Bank.loanValue);
            Updater.formatAndUpdateMoneyElement("displaySalary", person.personalEconomy);
        }
    }
}

export { Person };