import { Bank } from "./bank.js";

//Updater  mainly serves the purpose of making the element updates a bit more readable. 
class Updater {

    static updateElement(elName, value) {
        document.getElementById(elName).innerText = value;
    }
    static formatAndUpdateMoneyElement(elName, value) {
        document.getElementById(elName).innerText = Bank.formatCurrency(value);
    }
    static hideElement(elName) {
        document.getElementById(elName).style.display = "none";
    }
    static showElement(elName) {
        document.getElementById(elName).style.display = "block";
    }

}
export { Updater };