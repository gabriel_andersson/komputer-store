import { Validator } from "./validation.js";
import { Person } from "./person.js";
import { defaultComputer, computers } from "./laptop-manager.js";
import { Updater } from "./updater.js";

//References to html nodes
const workBtn = document.getElementById("work");
const bankBtn = document.getElementById("bankBtn");
const getLoanBtn = document.getElementById("getLoan");
const repayLoanBtn = document.getElementById("loanBtn");
const buyNowBtn = document.getElementById("buyNow");


//Instantiating a person and a computer object that acts as a placeholder that i can populate with the first incoming 
//computer from the array. That way i can make sure that a default is sent to the validateBuyNow() method when the buy button is
// clicked. When the imported default computer is not undefined anymore the selected computer gets sent to the validateBuyNow(). 
let gabriel = new Person();
let defaultComp = computers[0];

workBtn.addEventListener("click", () => {
    gabriel.addSalary(gabriel);
});

bankBtn.addEventListener("click", () => {
    gabriel.Bank.bankTransfer(gabriel);
});

//Calling repay loan functionality, felt kinda logical to let the Person class handle that. Also feels nice to avoid overcrowding the bank.js file
repayLoanBtn.addEventListener("click", () => {
    Person.repayLoan(gabriel);
});

//Prompting user here and using some array destructuring to handle the return values from the getLoan() method.
//If no error occurs the Updater class then uses the deconstructed values to update som html elements. 
//If an error occurs an alert is thrown with the relevant error message.
getLoanBtn.addEventListener("click", () => {

    const loanInput = prompt("Enter the amount you wish to loan");
    const [loanValue, error] = gabriel.Bank.getLoan(gabriel, loanInput);

    if (error) {
        alert(error);
        return;
    }
    Updater.showElement("loanBtn");
    Updater.showElement("loanBalance");

    Updater.formatAndUpdateMoneyElement("loanBalance", loanValue);
    Updater.formatAndUpdateMoneyElement("displayBankBalance", gabriel.Bank.bankBalance);
});

/*I have made the choice to put the eventlistener that listens to changes in the select element in the laptop-manager.js
since i wanted to keep related logic mostly in the same file. 
I kept the buyNowBtn listener in here in order to have all the buttonrelated listeners in one place. */ 
buyNowBtn.addEventListener("click", () => {

    if (typeof defaultComputer.title == "undefined") {

        Validator.validateBuyNow(gabriel, defaultComp);

        Updater.formatAndUpdateMoneyElement("displayBankBalance", gabriel.Bank.bankBalance);
    }
    else {
        Validator.validateBuyNow(gabriel, defaultComputer);

        Updater.formatAndUpdateMoneyElement("displayBankBalance", gabriel.Bank.bankBalance);
    }
});







