class Validator {
    //Checks if the loan asked for is bigger than twice the amount of the bank balance.
    static isLoanWithinBounds(bankBalance, loanAmount) {
        if (loanAmount > bankBalance * 2) {
            return false;
        }
        //Checks if the loan asked for is exactly twice the sum of the bank balance account OR if it is bigger than 0.
        else if (loanAmount == bankBalance * 2 || loanAmount > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    //Checks to make sure the userInput is valid as per the requested specifications.
    static validateLoanUserInput(userInput) {
        if (isNaN(userInput) || userInput <= 0 || userInput.length <= 0 || typeof userInput === "undefined") {
            return false;
        }
        else {
            return true;
        }
    }
    //Checks to see if person has enough funds on the bank balance account to make a purchase of the selected computer. 
    static validateBuyNow(person, computer) {
  
        if ((person.Bank.bankBalance - computer.price) >= 0) {  
            person.Bank.bankBalance = person.Bank.bankBalance - computer.price;
            alert(`Congratulations! You now own a ${computer.title}!`);
        } else {
            alert(`You do not have enough money in the bank to buy ${computer.title}`);
        }
    }
}

export { Validator };